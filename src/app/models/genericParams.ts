'use strict';

export interface GenericFilterParams {
  CurrentPage: number;
  PageSize: number;
  OrderProperty ?: string;
  Search ?: string;
  Ascending: boolean;
}

export interface GenericResponse {
  results: any[];
  currentPage: number;
  pageSize: number;
  rowCount: number;
}
