import { Component, Input, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { BaseComponent } from 'app/shared/product/base/base.component';
import { ProductService } from '../service.service';

@Component({
  selector: 'ngx-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent extends BaseComponent implements OnInit {

  @Input() public id: string = null;
  @Input() public name: string = null;

  constructor(protected service: ProductService,
    public ref: NbDialogRef<DetailComponent>) {
      super(service);
    }

  async ngOnInit() {
    if (this.id)
      await this.detail(this.id);
  }

}
