import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { BaseComponent } from 'app/shared/product/base/base.component';
import { CountryService } from 'app/shared/services/country.service';
import { ProductService } from '../service.service';

@Component({
  selector: 'ngx-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss'],
})
export class SummaryComponent extends BaseComponent implements OnInit {

  @Input() id: string = null;
  public form: FormGroup;
  public selectedItem: string = '';
  public countries: any = [];

  constructor(private route: ActivatedRoute,
    private router: Router,
    public fb: FormBuilder,
    private toastr: NbToastrService,
    private countryService: CountryService,
    protected service: ProductService) {
      super(service);

      this.form = this.fb.group({
        name: ['', Validators.required],
        price: [0.00, Validators.min(0)],
        stock: [0, Validators.min(0)],
        manufacturerName: ['', Validators.required],
        manufacturerCountry: ['', Validators.required],
        manufacturerEmail: ['', Validators.required],
        features: ['', Validators.nullValidator],
        thumbnail: ['', Validators.nullValidator],
      });
    }

  async ngOnInit() {
    this.getCountries();
    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id) {
      await this.detail(this.id);
      await this.setProduct(this.product);
    }
  }

  async getCountries() {
    await this.countryService.get().then((resp: any) => this.countries = resp);
  }

  async setProduct(product) {
    const { name, price, stock, manufacturerName, manufacturerEmail, thumbnail, manufacturerCountry } = product;
    this.form.patchValue({ name, price, stock, manufacturerName, manufacturerEmail, thumbnail, manufacturerCountry });
  }

  async submit() {
    if (this.form.invalid) {
      this.toastr.warning('Error en los valores ingresados en el formulario', 'Verificar formulario');
      return;
    }

    const payload = this.form.value;

    if (this.id) {
      payload.id = this.id;
      await this.service.update(this.id, payload)
        .then((resp: any) =>
          resp ? this.toastr.success(`El producto ${resp.name} se actualizo con exito`, 'Exito') : null);
    } else
      await this.service.create(payload)
        .then((resp: any) => {
          if (resp) {
            this.toastr.success(`El producto ${resp.name} se creo con exito`, 'Exito');
            this.router.navigateByUrl('/pages/products');
          }
        });
  }

}
