'use strict';

export const constants = {
  title: 'Lista de productos',
  text: {},
  endpoint: {
    base: 'product/',
    paged: 'product/getPaged',
  },
};
