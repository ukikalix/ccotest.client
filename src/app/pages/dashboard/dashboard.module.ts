import { NgModule } from '@angular/core';
import { NbCardModule } from '@nebular/theme';
import { SharedModule } from 'app/shared/shared.module';
import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { DetailComponent } from './detail/detail.component';
import { SummaryComponent } from './summary/summary.component';

@NgModule({
  imports: [
    NbCardModule,
    ThemeModule,
    SharedModule,
  ],
  exports: [],
  declarations: [
    DashboardComponent,
    DetailComponent,
    SummaryComponent,
  ],
  entryComponents: [DetailComponent],
})
export class DashboardModule { }
