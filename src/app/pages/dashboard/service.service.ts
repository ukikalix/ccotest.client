import { Injectable } from '@angular/core';
import { GenericFilterParams } from 'app/models/genericParams';
import { ConfigService } from 'app/services/config.service';
import { constants } from './dashboard.constants';

@Injectable({
  providedIn: 'root',
})
export class ProductService {

  public constants = constants;

  constructor(private config: ConfigService) { }

  async get(payload: GenericFilterParams) {
    return await this.config.get(this.config.builQueryNoFriendly(this.constants.endpoint.paged, payload));
  }

  async detail(id) {
    return await this.config.get(this.config.builQuery(this.constants.endpoint.base, { id }));
  }

  async create(payload) {
    return await this.config.post(this.constants.endpoint.base, payload);
  }

  async update(id, payload) {
    return await this.config.put(this.config.builQuery(this.constants.endpoint.base, {id}), payload);
  }

  async delete(id: string | number) {
    return await this.config.delete(this.config.builQuery(this.constants.endpoint.base, {id}));
  }
}
