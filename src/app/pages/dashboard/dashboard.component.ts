import { Component, OnInit } from '@angular/core';
import { GenericFilterParams, GenericResponse } from 'app/models/genericParams';
import { constants } from './dashboard.constants';
import { ProductService } from './service.service';

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {

  public constants = constants;
  public filterParams: GenericFilterParams = {
    CurrentPage: 1,
    PageSize: 4,
    OrderProperty: 'Name',
    Ascending: true,
  };
  public selectedItem = 'Name';
  public selectedSize = '4';
  public products: GenericResponse = null;
  public search: string = null;
  public showButtons: boolean = true;

  constructor(private service: ProductService) {
  }

  async ngOnInit() {
    await this.getProducts(this.filterParams);
  }

  async getProducts(payload: GenericFilterParams) {
    await this.service.get(payload).then((resp: GenericResponse) => this.products = resp);
  }

  async searchEvent() {
    if (this.search === null)
      return;
    else
      this.filterParams.Search = this.search;
      await this.getPage(1);
  }

  async getPage(page: number) {
    this.filterParams.CurrentPage = page;
    await this.getProducts(this.filterParams);
  }

  async refresh(event) {
    if (event)
      await this.getPage(1);
  }

  async delete(event) {
    if (event)
      await this.service.delete(event);
  }

  async filterChange(event) {
    this.filterParams.OrderProperty = event;
    await this.getPage(1);
  }

  async asc(event) {
    this.filterParams.Ascending = event;
    await this.getPage(1);
  }

  async pageSizeChange(event) {
    this.filterParams.PageSize = event;
    await this.getPage(1);
  }
}
