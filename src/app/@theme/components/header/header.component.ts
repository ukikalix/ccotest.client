import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserData } from '../../../@core/data/users';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {

  private destroy$: Subject<void> = new Subject<void>();
  public name: string = 'CCO Test Client';
  user: any;
  userMenu = [ { title: 'Log out' } ];

  constructor(private router: Router,
              private userService: UserData) {
  }

  ngOnInit() {
    this.userService.getUsers()
    .pipe(takeUntil(this.destroy$)).subscribe((users: any) => this.user = users.ivan);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  navigateHome() {
    this.router.navigateByUrl('/pages/products');
  }
}
