import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">
      Developed test by <b><a href="https://ukikalix.github.io/IvanLaraCV/" target="_blank">Ivan Lara</a></b> {{ date }}
    </span>
    <div class="socials">
      <a href="https://ukikalix.github.io/IvanLaraCV/" target="_blank" class="ion ion-social-github"></a>
      <a href="https://www.linkedin.com/in/ivan-antonio-lara-kalix-88b100106/" target="_blank" class="ion ion-social-linkedin"></a>
    </div>
  `,
})
export class FooterComponent {
  public date: string = new Date().getFullYear().toString();
}
