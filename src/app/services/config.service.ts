import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';


@Injectable({
  providedIn: 'root',
})
export class ConfigService {

  private urlHostAPI: string;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    }),
  };

  private httpOptionsFile = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
    }),
  };

  constructor(private http: HttpClient) {
    this.urlHostAPI = environment.url_host_api;
  }

  get(url: string, avoidHostApi: boolean = false) {
    if (avoidHostApi)
      return this.http.get(`${url}`).toPromise();

    this.setHeaders();
    return this.http.get(`${this.urlHostAPI}/${url}`, this.httpOptions).toPromise();
  }

  post(url: string, payload: object) {
    this.setHeaders();
    return this.http.post(`${this.urlHostAPI}/${url}`, payload, this.httpOptions).toPromise();
  }

  postFile(url: string, payload: object) {
    this.setHeadersFile();
    return this.http.post(`${this.urlHostAPI}/${url}`, payload, this.httpOptionsFile).toPromise();
  }

  put(url: string, payload: object) {
    this.setHeaders();
    return this.http.put(`${this.urlHostAPI}/${url}`, payload, this.httpOptions).toPromise();
  }

  delete(url: string) {
    this.setHeaders();
    return this.http.delete(`${this.urlHostAPI}/${url}`, this.httpOptions).toPromise();
  }

  setHeaders() {
    const token: any = JSON.parse(localStorage.getItem('auth_app_token')) || '';
    this.httpOptions.headers = this.httpOptions.headers.set(
      'Authorization',
      `Bearer ${token.value}`,
    );
  }

  setHeadersFile() {
    const token: any = JSON.parse(localStorage.getItem('auth_app_token')) || '';
    this.httpOptionsFile.headers = this.httpOptionsFile.headers.set(
      'Authorization',
      `Bearer ${token.value}`,
    );
  }

  builQuery(url: string, params: any) {
    const queryParams = Object.keys(params)
      .map(key => {
        return encodeURIComponent(params[key] != null ? params[key] : '');
      }).join('/');
    return `${url}${queryParams}`;
  }

  builQueryNoFriendly(url: string, params: any) {
    const queryParams = Object.keys(params)
      .map(key => {
        return encodeURIComponent(key) + '=' + encodeURIComponent((params[key] != null ? params[key] : ''));
      }).join('&');
    return `${url}?${queryParams}`;
  }
}
