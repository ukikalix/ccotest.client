import { Injectable } from '@angular/core';
import { ConfigService } from 'app/services/config.service';
import { constants } from './country.constants';

@Injectable({
  providedIn: 'root',
})
export class CountryService {

  public constants = constants;

  constructor(private config: ConfigService) { }

  async get() {
    return await this.config.get(this.constants.endpoint.base, true);
  }
}
