import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {
  NbIconModule,
  NbSelectModule,
  NbCardModule,
  NbInputModule,
  NbTabsetModule,
  NbUserModule,
  NbListModule,
  NbSearchModule,
  NbTreeGridModule,
  NbAlertModule,
  NbDatepickerModule,
  NbPopoverModule,
  NbActionsModule,
  NbBadgeModule,
  NbCheckboxModule,
  NbAccordionModule,
  NbContextMenuModule,
  NbRadioModule,
  NbTooltipModule,
  NbSpinnerModule,
  NbButtonModule,
} from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { NgxPaginationModule } from 'ngx-pagination';
import { ProductComponent } from './product/product.component';
import { BaseComponent } from './product/base/base.component';

const SHAREDMODULES: any[] = [
  FormsModule,
  ReactiveFormsModule,
  NbEvaIconsModule,
  NbIconModule,
  NbSelectModule,
  NbCardModule,
  NbInputModule,
  NbTabsetModule,
  NbUserModule,
  NbListModule,
  NbSearchModule,
  NbTreeGridModule,
  NbAlertModule,
  NbDatepickerModule,
  NbPopoverModule,
  NgxPaginationModule,
  NbActionsModule,
  NbBadgeModule,
  NbCheckboxModule,
  NbAccordionModule,
  NbContextMenuModule,
  NbRadioModule,
  NbTooltipModule,
  NbSpinnerModule,
  NbButtonModule,
  RouterModule,
];

const SHAREDCOMPONENTS: any[] = [
  ProductComponent,
];

@NgModule({
  declarations: [SHAREDCOMPONENTS, BaseComponent],
  imports: [
    CommonModule,
    SHAREDMODULES,
  ],
  exports: [
    SHAREDMODULES,
    SHAREDCOMPONENTS,
  ],
})
export class SharedModule { }
