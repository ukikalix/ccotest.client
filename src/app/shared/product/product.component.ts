import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { DetailComponent } from 'app/pages/dashboard/detail/detail.component';

@Component({
  selector: 'ngx-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {

  @Input() public product: any = null;
  @Input() showButtons: boolean = false;
  @Output() private refreshEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() private deleteEvent: EventEmitter<any> = new EventEmitter<any>();

  constructor(private dialogService: NbDialogService) { }

  ngOnInit(): void {
  }

  detail(id: string, name: string) {
    this.dialogService.open(DetailComponent, {
      closeOnEsc: true,
      hasScroll: true,
      closeOnBackdropClick: false,
      autoFocus: true,
      context: { id, name },
    }).onClose.subscribe((response) => this.refreshEvent.emit(response));
  }

  delete(id: string) {
    this.deleteEvent.emit(id);
  }

}
