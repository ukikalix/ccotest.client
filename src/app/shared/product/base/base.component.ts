import { Component, OnInit } from '@angular/core';
import { ProductService } from 'app/pages/dashboard/service.service';

@Component({
  selector: 'ngx-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss'],
})
export class BaseComponent implements OnInit {

  public product: any = null;

  constructor(protected service: ProductService) { }

  async ngOnInit() {
  }

  async detail(id: string) {
    await this.service.detail(id).then((resp: any) => this.product = resp);
  }

}
