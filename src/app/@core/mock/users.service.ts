import { of as observableOf,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Contacts, RecentUsers, UserData } from '../data/users';

@Injectable()
export class UserService extends UserData {

  private time: Date = new Date;

  private users = {
    ivan: { name: 'Ivan Lara', picture: 'https://instagram.fmga2-1.fna.fbcdn.net/v/t51.2885-19/s150x150/136773022_315923316439226_1271253242567983182_n.jpg?_nc_ht=instagram.fmga2-1.fna.fbcdn.net&_nc_ohc=viEnRzqNU3IAX-0aWag&tp=1&oh=c22eb165fa3a3a05011df37300ad21b7&oe=60470945' },
  };
  private types = {
    mobile: 'mobile',
    home: 'home',
    work: 'work',
  };
  private contacts: Contacts[] = [
    { user: this.users.ivan, type: this.types.mobile },
  ];
  private recentUsers: RecentUsers[]  = [
    { user: this.users.ivan, type: this.types.home, time: this.time.setHours(21, 12)},
  ];

  getUsers(): Observable<any> {
    return observableOf(this.users);
  }

  getContacts(): Observable<Contacts[]> {
    return observableOf(this.contacts);
  }

  getRecentUsers(): Observable<RecentUsers[]> {
    return observableOf(this.recentUsers);
  }
}
