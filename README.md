# TEST IVAN LARA

El proyecto utiliza ASP.Net Core 3.1, EF Core y MSSQL-Server(LocalDb)
en el API, y Angular 10 en el cliente.

## API
Basta con ejecutar el proyecto, para que este ejecute el DbContext,
e inicialice las entidades en la base de datos de _LocalDb_.

## Cliente
Se necesita ejecutar el comando **_npm i_** en una consola, y luego el
comando **_ng serve -o_** para abrirlo en el navegador, con el API previamente ejecutada.

[LinkedIn](https://www.linkedin.com/in/ivan-antonio-lara-kalix-88b100106/)
